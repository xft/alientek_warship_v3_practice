//
// Created by T on 2018/11/16.
//

#include "stm32f1xx_hal.h"

//#define PUTC_UART USART2
#define PUTC_UART USART1

#ifdef __GNUC__
    /* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf set to 'Yes') calls __io_putchar() */
    #define PUTCHAR_PROTOTYPE() int __io_putchar(int ch)
#else
    #define PUTCHAR_PROTOTYPE() int fputc(int ch, FILE *f)
#endif /* __GNUC__ */
PUTCHAR_PROTOTYPE() {
    while ((PUTC_UART->SR & USART_SR_TC) == 0);
    PUTC_UART->DR = ch & 0xFF;
    return ch;
}
