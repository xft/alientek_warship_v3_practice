//
// Created by T on 2018/11/26.
//

#include "task.h"
#include <stdint.h>
#include <stddef.h>
#include <pkt.h>
#include "led.h"
#include "esp8266.h"
#include "utils.h"


static const task_t *tasks[] = {
        &esp8266_task,
        &led_task,
        &pkt_task,
};


static void run_one(task_t *task) {
    if (TASK_IS_READY(task)) {
        TASK_SET_STAT_TO(task, TASK_FLAG_STAT_ACTIV);
        task->run();
        TASK_SET_STAT_TO(task, TASK_FLAG_STAT_READY);
    }
}

void task_schedule_once(void) {
    int i;
    for (i = 0; i < ARR_SIZE(tasks); ++i) {
        run_one((task_t *) tasks[i]);
    }
}

void task_schedule_forever(void) {
    while (1) {
        task_schedule_once();
    }
}
