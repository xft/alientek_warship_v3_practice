//
// Created by T on 2018/11/14.
//

#include "xtime.h"

static struct {
    volatile time_t  tv_sec;  /* seconds */
    volatile int32_t tv_msec; /* and milliseconds */
} _t;

void HAL_SYSTICK_Callback(void)
{
    _t.tv_msec += DURATION_MILLISECOND;
    if (_t.tv_msec >= DURATION_SECOND) {
        _t.tv_sec++;
        _t.tv_msec -= DURATION_SECOND;
    }
}


time_t gettimeseconds(time_t *t)
{
    time_t sec = _t.tv_sec;
    if (t) {
        *t = sec;
    }
    return sec;
}

void gettimev(struct timev *tv)
{
    if (tv) {
        tv->tv_msec = _t.tv_msec;
        tv->tv_sec = _t.tv_sec;
    }
}

void settimev(struct timev *tv)
{
    if (tv) {
        _t.tv_msec = tv->tv_msec;
        _t.tv_sec = tv->tv_sec;
    }
}
