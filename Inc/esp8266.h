//
// Created by T on 2019-03-14.
//

#ifndef __ESP8266_H__
#define __ESP8266_H__

#include <stdint.h>
#include "hal_uart.h"
#include "task.h"

struct iovec {
    const char *iov_base;   // Base address
    size_t iov_len;         // Length
};

char *HAL_StatusTypeDef_str(HAL_StatusTypeDef status);

HAL_StatusTypeDef esp8266_init(const char *ssid, const char *psk, const char *server_host, int server_port);
int esp8266_tcp_connected(void);
typedef void (*net_send_done_cb_t)(HAL_StatusTypeDef status, void *udata);
HAL_StatusTypeDef esp8266_net_send(const void *data, size_t len, net_send_done_cb_t done_cb, void *udata);
HAL_StatusTypeDef esp8266_net_sendv(const struct iovec *iov, int iovcnt, net_send_done_cb_t done_cb, void *udata);

extern task_t esp8266_task;

#endif //__ESP8266_H__
