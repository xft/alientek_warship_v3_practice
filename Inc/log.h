#ifndef __LOG_H__
#define __LOG_H__

#define USE_LOG 1

#if USE_LOG
#define LOGd(fmt, args...) printf("[DBG] %s:%d: " fmt "\r\n", __FILE__, __LINE__, ## args)
#define LOGi(fmt, args...) printf("[INF] %s:%d: " fmt "\r\n", __FILE__, __LINE__, ## args)
#define LOGw(fmt, args...) printf("[WAR] %s:%d: " fmt "\r\n", __FILE__, __LINE__, ## args)
#define LOGe(fmt, args...) printf("[ERR] %s:%d: " fmt "\r\n", __FILE__, __LINE__, ## args)
#else
#define LOGd(fmt, args...)
#define LOGi(fmt, args...)
#define LOGw(fmt, args...)
#define LOGe(fmt, args...)
#endif

#endif /* __LOG_H__ */
