//
// Created by T on 2018/11/26.
//

#ifndef __TASK_H__
#define __TASK_H__

typedef void (*task_run_t)(void);

typedef struct task {
    volatile int flags;
    task_run_t run;
} task_t;

#define TASK_FLAG_STAT_SLEEP    0
#define TASK_FLAG_STAT_READY    1
#define TASK_FLAG_STAT_ACTIV    2
#define TASK_FLAG_STAT_MASK     3

#define TASK_SET_STAT_TO(task, stat) \
	((task)->flags = ((task)->flags & ~TASK_FLAG_STAT_MASK) | (stat))

#define TASK_IS_READY(task) \
	((task)->flags & TASK_FLAG_STAT_MASK) == TASK_FLAG_STAT_READY

void task_schedule_once(void);
void task_schedule_forever(void);

#endif // __TASK_H__

