//
// Created by T on 2018/11/26.
//

#ifndef __HAL_UART_H__
#define __HAL_UART_H__

#include "fifo.h"

extern UART_HandleTypeDef huart2;
extern UART_HandleTypeDef huart3;

extern struct fifo uart1_fifo;
extern struct fifo uart2_fifo;
extern struct fifo uart3_fifo;

void uart_Enable_Receive_IT(UART_HandleTypeDef *huart);

void uart_IRQHandler(UART_HandleTypeDef *huart, struct fifo *fifo);

#endif // __HAL_UART_H__
