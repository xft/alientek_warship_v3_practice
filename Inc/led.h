//
// Created by T on 2018/11/16.
//

#ifndef __LED_H__
#define __LED_H__

#include <stdint.h>
#include "task.h"

#define LED_MAX_LEDS            2

#define LED_0                   0x01
#define LED_1                   0x02
#define LED_ALL                 (LED_0 | LED_1)

#define LED_MODE_OFF            0x00
#define LED_MODE_ON             0x01
#define LED_MODE_BLINK          0x02
#define LED_MODE_FLASH          0x04
#define LED_MODE_TOGGLE         0x08

#define LED_DEFAULT_DUTY_CYCLE  50
#define LED_DEFAULT_FLASH_COUNT 50
#define LED_DEFAULT_FLASH_TIME  1000


#define LED_INIT                LED_1
#define LED_RUNNING             LED_2


void led_init(void);
void led_set(uint8_t leds, uint8_t mode, ...);
uint8_t led_get_mode(uint8_t led);
void led_raw_on_off(uint8_t leds, uint8_t mode);


extern task_t led_task;

#endif /* __LED_H__ */

