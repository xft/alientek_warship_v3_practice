//
// Created by T on 2018/11/26.
//

#ifndef __PKT_H__
#define __PKT_H__

#include "stm32f1xx_hal.h"
#include "task.h"

#ifdef __IAR_SYSTEMS_ICC__
#pragma pack(push) //保存对齐状态
#pragma pack(1) //设定为1字节对齐
#else
# ifndef __GNUC__
// KEIL
__packed
# endif
#endif
struct pkt {
    uint8_t cmd;
    uint16_t param_len;
    uint8_t param[1];
    /* uint8_t CHECKSUM */
}
#ifdef __GNUC__
    __attribute__((packed))
#endif
;
#ifdef __IAR_SYSTEMS_ICC__
#pragma pack(pop) //保存对齐状态
#endif

#define PKT_MIN_SIZE                (sizeof(struct pkt))
#define PKT_MAX_SIZE                (PKT_MIN_SIZE + 68U + 1024U)
#define PKT_HEAD_SIZE               (PKT_MIN_SIZE - 1 /* CHECKSUM 1 byte*/)
#define PKT_SIZE(pktptr)            ((pktptr)->param_len + PKT_MIN_SIZE)
#define PKT_CHECKSUM_BYTE(pktptr)   ((pktptr)->param[(pktptr)->param_len])


uint8_t calc_bcc(uint8_t leading, const void *dat, size_t len);


#define CMD_LOGIN           0x00
#define CMD_REPORT_MEASURE  0x01
#define CMD_REPORT_WAVE     0x02
#define CMD_REPORT          0x03
#define CMD_ON_OFF_CTL      0x40


void on_tcp_connected(void);
void depkt_and_handle(int channel, const void *data, size_t size);
HAL_StatusTypeDef report_measure(const void *data, size_t size);
HAL_StatusTypeDef report_wave(const void *data, size_t size);
HAL_StatusTypeDef report_all(const void *data, size_t size);

extern task_t pkt_task;

#endif //__PKT_H__
